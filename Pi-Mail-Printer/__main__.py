#!/usr/bin/env python3
from . import PiMailPrinter
import time
"""
This script prints new mails to a specified network printer
Version: 1.0
Python 3.9+
Date created: June 5th, 2023
Date modified: -
"""


import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


def main():
    printer = PiMailPrinter.PiMailPrinter()
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
