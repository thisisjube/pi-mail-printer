# Pi-Mail-Printer

This repository contains a python script which prints new emails
for a specified mail account to a network printer. It runs as a systemd service, suitable
for a raspberry pi for example. Set up a .env file with your credentials etc. as described below
and install the required tools/ dependencies.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

Set up the file ```Pi-Mail-Printer/.env``` which defines the following values:
```HOSTNAME``` (e.g. imap.ionos.de), ```MAILBOX``` (the folder which is checked for new mails),
```MAIL_USER``` (username for your mail account), ```PASSWORD``` (password for your mail account), 
```PRINTER_NAME``` (name of the printer in your network), 
```FILEPATH``` (filepath where .html and .pdf versions of your mails are stored)

### Installation

- install ```wkhtmltopdf``` with ```sudo apt install wkhtmltopdf``` to ```/usr/bin```
- install python3 with ```sudo apt install python3```
- install pip3 with ```sudo apt install pip3```
- install python requirements with ```pip3 install -r requirements.txt```
- Change the paths in ``pi-mail-printer.service`` according to your file structure
- copy ```pi-mail-printer.service``` to ```/etc/systemd/system``` to install this script as a systemd service
- execute ``systemctl daemon-reload``
- execute ``systemctl enable pi-mail-printer.service``

After these steps the script should run as a service in the background and incoming mails should be printed
automatically.

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

Justus Bergermann - [Twitter](https://mobile.twitter.com/thisisjube) - Or just open an issue <br>
Project Link: [https://gitlab.com/thisisjube/pi-mail-printer](https://gitlab.com/thisisjube/pi-mail-printer)

<p align="right">(<a href="#readme-top">back to top</a>)</p>
