import time
from imapclient import IMAPClient, SEEN
import os
import cups
import pyzmail
import datetime
import subprocess
from dotenv import load_dotenv


class PiMailPrinter:
    def __init__(self):
        self.get_credentials()
        self.client = IMAPClient(self.HOSTNAME)
        self.client.login(self.MAIL, self.PASSWORD)
        self.MAIL_CHECK_FREQ = 10
        self.start()

    def get_credentials(self):
        load_dotenv()
        self.MAIL = os.getenv("MAIL_USER")
        self.PASSWORD = os.getenv("PASSWORD")
        self.HOSTNAME = os.getenv("HOSTNAME")
        self.MAILBOX = os.getenv("MAILBOX")
        self.FILEPATH = os.getenv("FILEPATH")

    def print_pdf(self, filepath):
        conn = cups.Connection()
        printers = conn.getPrinters()
        printer_name = list(printers.keys())
        for i, j in enumerate(printer_name):
            if j == self.PRINTER_NAME:
                printer_num = i
        conn.printFile(printer_name[printer_num], filepath, '', {'landscape': 'False', 'cpi': '14', 'lpi': '10'})

    def check_mails(self):
        self.client.select_folder(self.MAILBOX)
        messages = self.client.search(["UNSEEN"])
        for uid in messages:
            self.client.add_flags(uid, [SEEN])
            raw_message = self.client.fetch([uid], ["BODY[]"])[uid][b"BODY[]"]
            message = pyzmail.PyzMessage.factory(raw_message)
            timestamp = datetime.datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
            print("New E-Mail from " + timestamp)
            text_part = message.html_part
            if text_part:
                body = text_part.get_payload().decode(text_part.charset)
                filepathhtml = self.FILEPATH + timestamp + ".html"
                filepathpdf = self.FILEPATH + timestamp + ".pdf"
                with open(filepathhtml, 'w', encoding='utf-8') as f:
                    f.write(body)
                subprocess.call(["/usr/bin/wkhtmltopdf", filepathhtml, filepathpdf])
                self.print_pdf(filepathpdf)
            else:
                print("Unable to retrieve email body.")

    def start(self):
        while True:
            self.check_mails()
            time.sleep(1)


if __name__ == '__main__':
    printer = PiMailPrinter()
    while True:
        time.sleep(1)
